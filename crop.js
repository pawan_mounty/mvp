const sharp = require('sharp');

let originalImage = './assets/original.png';
let outputImage = './assets/cropped.png';

sharp(originalImage)
  .extract({ width: 500, height: 400, left: 5, top: 5 })
  .toFile(outputImage)
  .then(function(new_file_info) {
    console.log('Image cropped and saved');
  })
  .catch(function(err) {
    console.log(err);
    console.log('An error occured');
  });
