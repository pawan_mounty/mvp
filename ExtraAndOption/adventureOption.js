const xlsx = require('xlsx');
const axios = require('axios');
const workbook = xlsx.readFile('../assets/ExtraAndOptions.xlsx');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[4]]);

const localURL = 'http://localhost:3004/options/';
const devUrl = 'http://dev-kube.admin.mounty.co/adventures/options';

let difficulty = {
  name: 'difficulty',
  values: []
};

for (const value of data) {
  if (value.Difficulty) {
    difficulty.values.push(value.Difficulty);
  }
}

axios
  .post(devUrl, difficulty, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error);
  });
