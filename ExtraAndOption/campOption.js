const xlsx = require('xlsx');
const axios = require('axios');
const workbook = xlsx.readFile('../assets/ExtraAndOptions.xlsx');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[1]]);

const localURL = 'http://localhost:3002/options/';
const devUrl = 'http://dev-kube.admin.mounty.co/camps/options';

let grade = {
  name: 'grade',
  values: []
};
let parking = {
  name: 'parking',
  values: []
};
let washroom = {
  name: 'washroom',
  values: []
};
let room = {
  name: 'room',
  values: []
};
let class1 = {
  name: 'class',
  values: []
};

let roomType = {
  name: 'roomType',
  values: []
};

for (const value of data) {
  if (value.GRADE) {
    grade.values.push(value.GRADE);
  }
  if (value.PARKING) {
    parking.values.push(value.PARKING);
  }
  if (value.Washroom) {
    washroom.values.push(value.Washroom);
  }
  if (value.campRooms) {
    room.values.push(value.campRooms);
  }
  if (value.class) {
    class1.values.push(value.class);
  }
  if (value.roomType) {
    roomType.values.push(value.roomType);
  }
}

axios
  .post(devUrl, room, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error.response.data);
  });

axios
  .post(devUrl, grade, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error.response.data);
  });

axios
  .post(devUrl, parking, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error.response.data);
  });

axios
  .post(devUrl, washroom, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error.response.data);
  });

axios
  .post(devUrl, class1, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error.response.data);
  });

axios
  .post(devUrl, roomType, {
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
    }
  })
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
  })
  .catch(error => {
    console.log('\n\n\n\n\n', error.response.data);
  });
