const xlsx = require('xlsx');
const axios = require('axios');
const workbook = xlsx.readFile('../assets/ExtraAndOptions.xlsx');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[3]]);

let terrains = [];
let inclusions = [];
let exclusions = [];
let activities = [];
let thingsToCarry = [];
let tags = [];

const localURL = 'http://localhost:3004/extras/';
const devUrl = 'http://dev-kube.admin.mounty.co/adventures/extras';

for (const value of data) {
  if (value.terrains) {
    terrains.push(value.terrains);
  }
  if (value.inclusions) {
    inclusions.push(value.inclusions);
  }
  if (value.activities) {
    activities.push(value.activities);
  }
  if (value.exclusions) {
    exclusions.push(value.exclusions);
  }
  if (value.thingToCarry) {
    thingsToCarry.push(value.thingToCarry);
  }
  if (value.Tag) {
    tags.push(value.Tag);
  }
}

for (const value of tags) {
  const tag = {
    type: 'tags',
    text: value
  };

  axios
    .post(devUrl, tag, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of terrains) {
  const terrain = {
    type: 'terrains',
    text: value
  };

  axios
    .post(devUrl, terrain, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of inclusions) {
  const inclusion = {
    type: 'inclusions',
    text: value
  };

  axios
    .post(devUrl, inclusion, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of exclusions) {
  const exclusion = {
    type: 'exclusions',
    text: value
  };

  axios
    .post(devUrl, exclusion, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of thingsToCarry) {
  const thingToCarry = {
    type: 'thingsToCarry',
    text: value
  };

  axios
    .post(devUrl, thingToCarry, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of activities) {
  let activity = {
    type: 'activities',
    text: value
  };

  axios
    .post(devUrl, activity, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}
