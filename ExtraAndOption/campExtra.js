const xlsx = require('xlsx');
const axios = require('axios');
const workbook = xlsx.readFile('../assets/ExtraAndOptions.xlsx');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

let terrains = [];
let types = [];
let amenities = [];
let inclusions = [];
let exclusions = [];
let activities = [];
let thingsToCarry = [];
let roomAmenities = [];

const localURL = 'http://localhost:3002/extras/';
const devUrl = 'http://dev-kube.admin.mounty.co/camps/extras';

for (const value of data) {
  if (value.camp_type) {
    types.push(value.camp_type);
  }
  if (value.amenities) {
    amenities.push(value.amenities);
  }
  if (value.terrains) {
    terrains.push(value.terrains);
  }
  if (value.inclusion) {
    inclusions.push(value.inclusion);
  }
  if (value.activities) {
    if (value.description) {
      activities.push({
        text: value.activities,
        description: value.description
      });
    } else {
      activities.push({ text: value.activities });
    }
  }
  if (value.exclusion) {
    exclusions.push(value.exclusion);
  }
  if (value.thingsToCarry) {
    thingsToCarry.push(value.thingsToCarry);
  }
  if (value.roomAmenities) {
    roomAmenities.push(value.roomAmenities);
  }
}

for (const value of types) {
  const type = {
    type: 'types',
    text: value
  };

  axios
    .post(devUrl, type, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of terrains) {
  const terrain = {
    type: 'terrains',
    text: value
  };

  axios
    .post(devUrl, terrain, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of amenities) {
  const amenity = {
    type: 'amenities',
    text: value
  };

  axios
    .post(devUrl, amenity, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of inclusions) {
  const inclusion = {
    type: 'inclusions',
    text: value
  };

  axios
    .post(devUrl, inclusion, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of exclusions) {
  const exclusion = {
    type: 'exclusions',
    text: value
  };

  axios
    .post(devUrl, exclusion, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of thingsToCarry) {
  const thingToCarry = {
    type: 'thingsToCarry',
    text: value
  };

  axios
    .post(devUrl, thingToCarry, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of roomAmenities) {
  const roomAmenity = {
    type: 'roomAmenities',
    text: value
  };

  axios
    .post(devUrl, roomAmenity, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}

for (const value of activities) {
  let activity;

  if (value.description) {
    activity = {
      type: 'activities',
      text: value.text,
      description: value.description
    };
  } else {
    activity = {
      type: 'activities',
      text: value.text
    };
  }

  axios
    .post(devUrl, activity, {
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Njc2NjgxMDF9.bXAB8C6M764I3b34MRfoEe_NdGXVHAJHaX35AvpuRpg'
      }
    })
    .then(response => {
      if (response.data.error) {
        console.log(response.data);
      }
    })
    .catch(error => {
      console.log('\n\n\n\n\n', error.response.data);
    });
}
