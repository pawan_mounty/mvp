const xlsx = require('xlsx');
const axios = require('axios');
const workbook = xlsx.readFile('../assets/AdventureProduct.xlsx');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[4]]);

for (let i = 0; i < data.length; i++) {
  getAdventures(data[i].Code, async (err, res) => {
    err ? console.log(err) : '';
    if (res && data[i].Meta_Description) {
      res.seo['description'] = data[i].Meta_Description;
      await updateAdventureSeo(res._id, res);
    }
  });
}

let count = 0;

async function updateAdventureSeo(id, payload) {
  await axios
    .request({
      method: 'PUT',
      url: `http://localhost:4002/adventures/v1/${id}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
      },
      data: payload
    })
    .then(response => {
      if (response.error) {
        console.log('error');
      } else {
        console.log('done\t\t', count);
        count++;
      }
    })
    .catch(error => {
      console.log('\n\\n\n\n\n\nn\n\n', error.response.data);
    });
}

function getAdventures(code, res) {
  const filters = [{ key: 'code', value: code }];
  axios
    .get('http://localhost:4002/adventures/v1', {
      headers: {
        'Content-Type': 'application/json',
        authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
      },
      params: {
        filters: JSON.stringify(filters)
      }
    })
    .then(async function(response) {
      if (response.data.data[0] && response.data.data[0]._id) {
        await getSingleAdventure(
          response.data.data[0]._id,
          (err, singleResponse) => {
            if (err) {
              console.log(err);
            } else {
              return res(null, singleResponse.data);
            }
          }
        );
      }
    })
    .catch(function(error) {
      // handle error
      console.log(error.response.data);
    });
}

async function getSingleAdventure(id, res) {
  await axios({
    method: 'get',
    url: `http://localhost:4002/adventures/v1/${id}`,
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
    }
  })
    .then(response => {
      return res(null, response.data);
    })
    .catch(function(error) {
      return res(error);
    });
}
