const xlsx = require('xlsx');
const axios = require('axios');
const fs = require('fs');
const workbook = xlsx.readFile('../assets/adventure.xlsx');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[3]]);
let operators = [];
let count = 0;
let toBePosted = data.length;

console.log('Adventure Operator to be posted', toBePosted);
for (let i = 0; i < data.length; i++) {
	let tempOperator = {};
	getName(data[i].Full_Name, (err, res) => {
		tempOperator['firstName'] = res[0];
		tempOperator['lastName'] = res[1];
		tempOperator['active'] = true;
	});
	tempOperator['mobile'] = '+91' + data[i].Mobile_Number;
	tempOperator['email'] = data[i].Email_Id;
	tempOperator['address'] = {
		locality: data[i].AddressLine1,
		city: data[i].City,
		state: data[i].State,
		country: data[i].Country,
		pincode: data[i].Pincode
	};
	tempOperator['legal'] = {
		businessType: data[i].Registration_Type || 'TBD',
		businessName: data[i].Legal_Name_of_Business || 'TBD',
		pan: data[i].Pan_Card_Number,
		gstin: data[i].GSTIN_Number
	};
	// tempOperator['bank'] = {
	//   bankName: data[i].Bank_Name,
	//   type: 'Update',
	//   beneficiaryName: data[i].Beneficiary_Name,
	//   accountNumber: data[i].Account_Number,
	//   ifsc: data[i].IFSC_CODE
	// };
	tempOperator['progress'] = '1';
	operators.push(tempOperator);
}
let jsonData = {};
for (let i = 0; i < operators.length; i++) {
	axios
		.post('https://admin.mounty.co/operators/v1', operators[i], {
			headers: {
				'Content-Type': 'application/json',
				authorization:
					'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
			}
		})
		.then(function(response) {
			if (response.error) {
				console.log(response.data);
			}
			count++;
			jsonData[data[i].Mobile_Number] = response.data.data;

			if (count == toBePosted) {
				var jsonContent = JSON.stringify(jsonData);
				fs.writeFile('output1.json', jsonContent, 'utf8', function(err) {
					if (err) {
						console.log('An error occured while writing JSON Object to File.');
						return console.log(err);
					}

					console.log('JSON file has been saved.');
				});
			}
		})
		.catch(function(error) {
			count++;
			console.log('\n\n\n\n\n\n\n', data[i].Full_Name);
			console.log(error.response.data.errorDetails);
		});
}

function getName(req, res) {
	let arr = [];
	arr.push(
		req
			.split(' ')
			.slice(0, 1)
			.join(' ')
	);
	arr.push(
		req
			.split(' ')
			.slice(1)
			.join(' ')
	);
	return res(null, arr);
}
