const xlsx = require('xlsx');
const axios = require('axios');
const workbook = xlsx.readFile('../assets/adventure.xlsx');
const jsonData = require('./output1.json');
const sheet_name_list = workbook.SheetNames;
const data = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[4]]);
let adventure = [];

let predefinedLocation = {};
getPredefinedLocations((err, res) => {
	err ? console.log(err) : '';
	for (let i = 0; i < res.length; i++) {
		predefinedLocation[res[i].name] = res[i];
	}
	console.log('Adventures to be posted', data.length);
	for (let i = 0; i < data.length; i++) {
		let tempVariant = [];
		let tempAdventure = {
			operator: jsonData[data[i].Mobile_Number]
		};
		tempAdventure['active'] = true;
		tempAdventure['code'] = data[i].Code;
		tempAdventure['title'] = data[i].Title_of_Adventure;
		tempAdventure.refundableBefore = 7;

		if (String(data[i].active) == '1') {
			tempAdventure.active = true;
		} else {
			tempAdventure.active = false;
		}

		tempAdventure['location'] = {
			code: predefinedLocation[data[i].Location.trim()].code,
			name: predefinedLocation[data[i].Location.trim()].name,
			state: predefinedLocation[data[i].Location.trim()].state,
			region: predefinedLocation[data[i].Location.trim()].region,
			country: 'India'
		};
		getArrayComma(data[i].Terrain, (err, res) => {
			err ? console.log(err) : '';
			let tempTerrain = [];
			for (let i = 0; i < res.length; i++) {
				tempTerrain.push({ text: res[i] });
			}
			tempAdventure['terrains'] = tempTerrain;
		});
		getArray(data[i].Category, (err, res) => {
			err ? console.log(err) : '';
			let tempActivities = [];
			for (let i = 0; i < res.length; i++) {
				if (res[i] != '') tempActivities.push({ text: res[i] });
			}
			tempAdventure['activities'] = tempActivities;
		});
		tempAdventure['difficulty'] = data[i].Difficulty_Level;
		tempAdventure['address'] = {
			locality: data[i].Address_of_Adventure,
			city: data[i].City_2,
			state: data[i].State_2,
			country: 'India',
			pincode: data[i].Pincode_2,
			coordinates: [data[i].Longitude, data[i].Lattitude]
		};
		tempAdventure['media'] = {
			images: []
		};
		for (let num = 1; num <= parseInt(data[i].Total_Pics); num++) {
			tempAdventure['media'].images.push({
				url:
					'https://s3.ap-south-1.amazonaws.com/media-mounty/adventures/' +
					data[i].Code +
					'/' +
					num +
					'.jpg'
			});
		}
		if (data[i].Youtube_Video_Link) {
			tempAdventure['media']['videos'] = [{ url: data[i].Youtube_Video_Link }];
		}
		tempAdventure['description'] = data[i].Overview;
		getArray(data[i].Highlights, (err, res) => {
			err ? console.log(err) : '';
			tempAdventure['highlights'] = res;
		});

		getArray(data[i].Note, (err, res) => {
			err ? console.log(err) : '';
			let tempNote = [];
			for (let i = 0; i < res.length; i++) {
				if (res[i] != '') tempNote.push(res[i]);
			}
			tempAdventure['notes'] = tempNote;
		});

		tempAdventure['operatingHours'] = {
			days: [
				'Sunday',
				'Monday',
				'Tuesday',
				'Wednesday',
				'Thursday',
				'Friday',
				'Saturday'
			],
			from: '10:00 AM',
			to: '6:00 PM'
		};
		tempAdventure['currency'] = 'Rupees';

		getArray(data[i].Inclusions, (err, res) => {
			err ? console.log(err) : '';
			let tempInclusion = [];
			for (let i = 0; i < res.length; i++) {
				if (res[i] != '')
					tempInclusion.push({
						text: res[i]
					});
			}
			tempAdventure['inclusions'] = tempInclusion;
		});
		getArray(data[i].Exclusions, (err, res) => {
			err ? console.log(err) : '';
			let tempExclusion = [];
			for (let i = 0; i < res.length; i++) {
				if (res[i] != '')
					tempExclusion.push({
						text: res[i]
					});
			}
			tempAdventure['exclusions'] = tempExclusion;
		});
		if (data[i].Title_And_Small_Desc_1 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_1,
				description: data[i].Small_Description_1,
				duration: data[i].Duration_1,
				price: data[i].Pricing_1,
				mrp: data[i].MRP_1,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_1
			});
		}
		if (data[i].Title_And_Small_Desc_2 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_2,
				description: data[i].Small_Description_2,
				duration: data[i].Duration_2,
				price: data[i].Pricing_2,
				mrp: data[i].MRP_2,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_2
			});
		}
		if (data[i].Title_And_Small_Desc_3 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_3,
				description: data[i].Small_Description_3,
				duration: data[i].Duration_3,
				price: data[i].Pricing_3,
				mrp: data[i].MRP_3,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_3
			});
		}
		if (data[i].Title_And_Small_Desc_4 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_4,
				description: data[i].Small_Description_4,
				duration: data[i].Duration_4,
				price: data[i].Pricing_4,
				mrp: data[i].MRP_4,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_4
			});
		}
		if (data[i].Title_And_Small_Desc_5 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_5,
				description: data[i].Small_Description_5,
				duration: data[i].Duration_5,
				price: data[i].Pricing_5,
				mrp: data[i].MRP_5,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_5
			});
		}
		if (data[i].Title_And_Small_Desc_6 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_6,
				description: data[i].Small_Description_6,
				duration: data[i].Duration_6,
				price: data[i].Pricing_6,
				mrp: data[i].MRP_5,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_6
			});
		}
		if (data[i].Title_And_Small_Desc_7 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_7,
				description: data[i].Small_Description_7,
				duration: data[i].Duration_7,
				price: data[i].Pricing_7,
				mrp: data[i].MRP_7,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_7
			});
		}
		if (data[i].Title_And_Small_Desc_8 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_8,
				description: data[i].Small_Description_8,
				duration: data[i].Duration_8,
				price: data[i].Pricing_8,
				mrp: data[i].MRP_8,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_8
			});
		}
		if (data[i].Title_And_Small_Desc_9 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_9,
				description: data[i].Small_Description_9,
				duration: data[i].Duration_9,
				price: data[i].Pricing_9,
				mrp: data[i].MRP_9,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_9
			});
		}
		if (data[i].Title_And_Small_Desc_10 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_10,
				description: data[i].Small_Description_10,
				duration: data[i].Duration_10,
				price: data[i].Pricing_10,
				mrp: data[i].MRP_10,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_10
			});
		}
		if (data[i].Title_And_Small_Desc_11 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_11,
				description: data[i].Small_Description_11,
				duration: data[i].Duration_11,
				price: data[i].Pricing_11,
				mrp: data[i].MRP_11,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_11
			});
		}
		if (data[i].Title_And_Small_Desc_12 != undefined) {
			tempVariant.push({
				title: data[i].Title_And_Small_Desc_12,
				description: data[i].Small_Description_12,
				duration: data[i].Duration_12,
				price: data[i].Pricing_12,
				mrp: data[i].MRP_12,
				childDiscount: 0,
				count: 50,
				markup: data[i].Markup_Commission,
				minimumPersons: data[i].Minimum_Pax_Required,
				remark: data[i].Remarks_12
			});
		}
		tempAdventure['variants'] = tempVariant;
		adventure.push(tempAdventure);
	}
	for (let i = 0; i < 69; i++) {
		// // if(adventure[i].location.code == undefined)
		// prettier-ignore
		const headers = {
			"Content-Type": "application/json",
			"Authorization":
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE"
		};
		// console.log(adventure[i].notes);
		// prettier-ignore
		axios
			.request({
				method: 'POST',
				url: 'https://admin.mounty.co/adventures/v1',
				headers: headers,
				data: adventure[i]
			})
			.then(function(response) {
				if (response.error) {
					console.log(response.data);
				}
			})
			.catch(function(error) {
				console.log('\n\n\n\n\n\n\n', adventure[i].title);
				if (error.response.data) {
					console.log(error.response.data.errorDetails);
				} else {
					console.log(error);
				}
			});
	}
});

function getArray(req, res) {
	let arr = req.split('\n');
	return res(null, arr);
}

function getArrayComma(req, res) {
	let arr = req.split(',');
	return res(null, arr);
}

function getPredefinedLocations(res) {
	axios
		.get('https://admin.mounty.co/locations/v1', {
			headers: {
				'Content-Type': 'application/json',
				authorization:
					'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
			}
		})
		.then(function(response) {
			// handle success
			console.log('Got Locations', response.data.data.length);
			return res(null, response.data.data);
		})
		.catch(function(error) {
			// handle error
			console.log(error);
		});
}
