const axios = require('axios');
const jsonData = require('./finalOutput.json');

console.log('Camper to be updated', Object.keys(jsonData).length);

for (const key in jsonData) {
  if (jsonData.hasOwnProperty(key)) {
    const camperId = key;
    const referrerId = jsonData[key];
    axios({
      method: 'put',
      url: `http://dev-kube.admin.mounty.co/campers/v1/${camperId}`,
      data: {
        referredBy: referrerId
      }
    })
      .then(function(response) {
        if (response.error) {
          console.log(response.data);
        } else {
          console.log(response.data);
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }
}
