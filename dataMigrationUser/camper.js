const customer = require('./db_helper_mounty');
const user = require('./db_helper_user');
const loyality = require('./db_helper_loyality');
const axios = require('axios');
const fs = require('fs');
const jsonFile = require('./position.json');
const output = require('./output.json');

const localUrl = 'http://localhost:3008/campers/v1';
const devUrl = 'http://dev-kube.admin.mounty.co/campers/v1';
let count = 0;
let jsonData = {};
const index = jsonFile.index;
fs.writeFile(
  'position.json',
  JSON.stringify({ index: index + 1 }),
  'utf8',
  err => {
    if (err) {
      console.log('An error occured while writing JSON Object to File.');
    }
  }
);
const arr = [
  0,
  500,
  1000,
  1500,
  2000,
  2500,
  3000,
  3500,
  4000,
  4500,
  5000,
  5500,
  6000,
  6500,
  7000,
  7500,
  8000,
  8500,
  9000,
  9500,
  10000,
  10500,
  11000,
  11500,
  12000,
  12500,
  13000,
  13500,
  14000,
  14500,
  15000,
  15500,
  16000,
  16500,
  17000,
  17500,
  18000,
  18500,
  19000,
  19500,
  20000,
  20500,
  21000,
  21500,
  22000,
  22500,
  23000,
  23500,
  24000
];

customer.query(`SELECT * from customers limit ${arr[index]},500`, function(
  err,
  customers,
  fields
) {
  if (err) {
    return err;
  } else {
    let total = customers.length;
    for (let i = 0; i < customers.length; i++) {
      let camper = {};
      if (customers[i].name != customers[i].phone) {
        camper['firstName'] = customers[i].name;
      }
      camper['mobile'] = '+' + customers[i].isd_code + customers[i].phone;
      if (customers[i].email != null) {
        camper['email'] = customers[i].email;
      }
      camper['country'] = customers[i].country_name;
      getReferralCode(customers[i].uuid, (err, res) => {
        if (res.length != 0) {
          camper['referralCode'] = res[0].code;
        } else {
          camper['referralCode'] = 'TBD';
        }
        getSource(customers[i].uuid, (err, res) => {
          if (res.length != 0) {
            camper['source'] = res[0].source;
          } else {
            camper['source'] = 'app:android';
          }
          getWallet(customers[i].uuid, (err, res) => {
            let tempWallet = [];
            for (let j = res.length - 1; j >= 0; j--) {
              const temp = {
                date: res[j].date,
                description: res[j].description,
                amount: res[j].amount
              };
              tempWallet.push(temp);
            }
            if (tempWallet.length != 0) {
              camper['wallet'] = tempWallet;
            }
            camper['active'] = true;

            setTimeout(function() {
              axios
                .post(localUrl, camper, {
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization:
                      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
                  }
                })
                .then(response => {
                  if (response.data.error) {
                    count++;
                    console.log(response.data);
                  } else {
                    getReferredBy(customers[i].uuid, (err, res) => {
                      if (res.length != 0) {
                        jsonData[customers[i].uuid] = {
                          id: response.data.data,
                          referredBy: res[0].referrer_id
                        };
                      } else {
                        jsonData[customers[i].uuid] = {
                          id: response.data.data,
                          referredBy: 'none'
                        };
                      }
                      count++;
                      if (count == total) {
                        let jsonContent = JSON.stringify(
                          Object.assign(output, jsonData)
                        );
                        fs.writeFile(
                          'output.json',
                          jsonContent,
                          'utf8',
                          function(err) {
                            if (err) {
                              console.log(
                                'An error occured while writing JSON Object to File.'
                              );
                            }
                          }
                        );
                        console.log('Done');
                      }
                    });
                  }
                })
                .catch(function(error) {
                  count++;
                  console.log('\n\n\n\n\n', camper.mobile, '\n\n\n\n\n');

                  if (count == total) {
                    let jsonContent = JSON.stringify(
                      Object.assign(output, jsonData)
                    );
                    fs.writeFile('output.json', jsonContent, 'utf8', function(
                      err
                    ) {
                      if (err) {
                        console.log(
                          'An error occured while writing JSON Object to File.'
                        );
                      }
                    });
                    console.log('Done');
                  }
                });
            }, 100);
          });
        });
      });
    }
  }
});

function getReferredBy(req, res) {
  loyality.query(
    `SELECT referrer_id from referrals where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getWallet(req, res) {
  loyality.query(
    `SELECT wallet_histories.amount AS amount, wallet_histories.remark AS description, wallet_histories.created_at AS date FROM wallets JOIN wallet_histories ON wallets.id = wallet_histories.wallet_id where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getSource(req, res) {
  user.query(
    `SELECT via as source from user_data where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getReferralCode(req, res) {
  loyality.query(
    `SELECT code from referral_codes where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}
