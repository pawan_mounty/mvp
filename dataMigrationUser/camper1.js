const customer = require('./db_helper_mounty');
const user = require('./db_helper_user');
const loyality = require('./db_helper_loyality');
const axios = require('axios');
const fs = require('fs');

let count = 0;
let total = 0;
let x = 18000;
let y = 18100;
let inc = 100;
var interval = 3 * 1000;
let jsonData = {};

(async function loop(i) {
  // setTimeout(async function() {
  if (--i) {
    const [err, res] = await postData(x, y);
    console.log(res);
    // postData(x, y)
    //   .then(res => {
    //     // if (err == null) {
    //     console.log(res);
    //     // }
    //     // console.log(jsonData);
    //     x = y;
    //     y = y + inc;
    //     // console.log('\n\n\n\n\n', i);
    //     loop(i);
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });
  }
  // else {
  //     // var jsonContent = JSON.stringify(jsonData);
  //     // console.log(jsonData);
  //     // await fs.writeFile('output.json', jsonContent, 'utf8', function(err) {
  //     //   if (err) {
  //     //     console.log('An error occured while writing JSON Object to File.');
  //     //   }
  //     //   console.log('Data written');
  //     // });
  //   }
  // }, interval);
})(6);

// for (let i = 0; i < 5; i++) {
//   //   if (i == 0) {
//   //     postData(i, (i + 1) * 1000);
//   //   } else {
//   //     postData(i * 1000, (i + 1) * 1000);
//   //   }
//   postData(x, y, (err, res) => {
//     if (err == null) {
//       console.log(jsonData);
//       x = y;
//       y = y + inc;
//     }
//     // if (err == null) {

//     // }
//   });
//   // if (i == 0) {
//   //   postData(5 * 1000, 5 * 1000 + 100);
//   // } else {
//   //   postData(6000, 7000);
//   // }
// }

async function postData(lowerLimit, upperLimit) {
  await customer.query(
    `SELECT * from customers limit ${lowerLimit},${upperLimit}`,
    function(err, customers, fields) {
      if (err) {
        return err;
      } else {
        let jsonData = {};
        // return dataRes(null, customers.length);
        // console.log(customers.length);
        // count = 0;
        total = customers.length;
        (async () => {
          for (let i = 0; i < customers.length; i++) {
            let camper = {};
            if (customers[i].name != customers[i].phone) {
              camper['firstName'] = customers[i].name;
            }
            camper['mobile'] = '+' + customers[i].isd_code + customers[i].phone;
            if (customers[i].email != null) {
              camper['email'] = customers[i].email;
            }
            camper['country'] = customers[i].country_name;
            getReferralCode(customers[i].uuid, async (err, res) => {
              if (res.length != 0) {
                camper['referralCode'] = res[0].code;
              } else {
                camper['referralCode'] = 'TBD';
              }
              getSource(customers[i].uuid, async (err, res) => {
                if (res.length != 0) {
                  camper['source'] = res[0].source;
                } else {
                  camper['source'] = 'app:android';
                }
                getWallet(customers[i].uuid, async (err, res) => {
                  let tempWallet = [];
                  for (let j = res.length - 1; j >= 0; j--) {
                    const temp = {
                      date: res[j].date,
                      description: res[j].description,
                      amount: res[j].amount
                    };
                    tempWallet.push(temp);
                  }
                  if (tempWallet.length != 0) {
                    camper['wallet'] = tempWallet;
                  }
                  camper['active'] = true;
                  await axios
                    .post('http://dev-kube.admin.mounty.co/campers/v1', camper)
                    .then(async function(response) {
                      // console.log(error);
                      // if (response.error) {
                      //   console.log(response.data);
                      //   count++;
                      // }
                      // else {
                      // console.log(response);
                      await getReferredBy(
                        customers[i].uuid,
                        async (err, res) => {
                          if (res.length != 0) {
                            jsonData[customers[i].uuid] = {
                              id: response.data.data,
                              referredBy: res[0].referrer_id
                            };
                          } else {
                            jsonData[customers[i].uuid] = {
                              id: response.data.data,
                              referredBy: 'none'
                            };
                          }
                        }
                      );
                      // }
                    });
                  // .catch(function(error) {
                  //   // console.log(error);
                  //   return dataRes(error);
                  //   // return dataRes(error.response.data.errorDetails);
                  // });
                });
              });
            });
          }
        })();
        return null, jsonData;
      }
    }
  );
}

function getReferredBy(req, res) {
  loyality.query(
    `SELECT referrer_id from referrals where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getWallet(req, res) {
  loyality.query(
    `SELECT wallet_histories.amount AS amount, wallet_histories.remark AS description, wallet_histories.created_at AS date FROM wallets JOIN wallet_histories ON wallets.id = wallet_histories.wallet_id where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getSource(req, res) {
  user.query(
    `SELECT via as source from user_data where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getReferralCode(req, res) {
  loyality.query(
    `SELECT code from referral_codes where user_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}
