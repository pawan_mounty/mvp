const jsonData = require('./output.json');
const fs = require('fs');

let jsonContent = {};

for (const key in jsonData) {
	if (jsonData.hasOwnProperty(key)) {
		const val = jsonData[key];
		const camperId = val.id;
		if (val.referredBy != 'none') {
			const referrer = jsonData[val.referredBy];
			if (referrer != undefined) {
				jsonContent[camperId] = referrer.id;
			}
		}
	}
}

let json = JSON.stringify(jsonContent);

fs.writeFile('finalOutput.json', json, 'utf8', function(err) {
	if (err) {
		console.log('An error occured while writing JSON Object to File.');
	}

	console.log('Done');
});
