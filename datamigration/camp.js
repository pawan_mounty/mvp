const client = require('./db_helper');
const axios = require('axios');
const jsonData = require('./output2.json');
const fs = require('fs');

let count = 0;
let total = 0;
client.query('SELECT * from camps where active = 1', function(
	err,
	camps,
	fields
) {
	if (err) {
		console.log(err);
	} else {
		let campJson = {};
		let predefinedLocation = {};
		total = camps.length;
		console.log('Camps to be posted', camps.length);
		getPredefinedLocations((err, res) => {
			!false ? '' : console.log('PredefinedLocatoins', err);
			console.log(res.length + ' Locations');
			for (let m = 0; m < res.length; m++) {
				predefinedLocation[res[m].name] = res[m];
			}
			for (let i = 0; i < camps.length; i++) {
				let tempCamp = {
					operator: jsonData[camps[i].operator_id],
					displayName: camps[i].display_name,
					code: camps[i].mounty_code,
					legalName: camps[i].property_name,
					type: camps[i].star_rating,
					class: [camps[i].property_type],
					minimumCampers: camps[i].min_persons,
					active: true,
					refundableBefore: 7
				};
				let tempImages = [];
				let tempVideos = [];
				getPhotos(camps[i].uuid, (err, res) => {
					!false ? '' : console.log('Photos', err);
					for (let j = 0; j < res.length; j++) {
						tempImages.push({
							url:
								'https://s3.ap-south-1.amazonaws.com/media-mounty/' +
								res[j].url,
							caption: res[j].caption
						});
					}
					getVideos(camps[i].uuid, (err, res) => {
						!false ? '' : console.log('Videos', err);
						for (let j = 0; j < res.length; j++) {
							tempVideos.push({
								url: res[j].url,
								caption: res[j].caption
							});
						}
						tempCamp['media'] = {
							images: tempImages,
							videos: tempVideos
						};
						getDescription(camps[i].uuid, (err, res) => {
							!false ? '' : console.log('Decsription', err);
							tempCamp['description'] = res[0].description;
							getAmenities(camps[i].uuid, (err, res) => {
								!false ? '' : console.log('Amenities', err);
								let tempAminities = [];
								for (let j = 0; j < res.length; j++) {
									tempAminities.push({
										text: res[j].text,
										icon: res[j].icon
									});
								}
								tempCamp['amenities'] = tempAminities;
								getHighlights(camps[i].uuid, (err, res) => {
									!false ? '' : console.log('Highlights', err);
									let tempHighlights = [];
									for (let j = 0; j < res.length; j++) {
										tempHighlights.push(res[j].description);
									}
									tempCamp['highlights'] = tempHighlights;
									getInclusions(camps[i].uuid, (err, res) => {
										!false ? '' : console.log('Inclusions', err);
										let tempInclusions = [];
										for (let j = 0; j < res.length; j++) {
											tempInclusions.push({ text: res[j].text });
										}
										tempCamp['inclusions'] = tempInclusions;
										getDirection(camps[i].uuid, (err, res) => {
											!false ? '' : console.log('Direction', err);
											let tempDirection = [];
											for (let j = 0; j < res.length; j++) {
												tempDirection.push({
													mode: res[j].mode,
													description: res[j].description
												});
											}
											tempCamp['directions'] = tempDirection;
											getThingsToCarry(camps[i].uuid, (err, res) => {
												!false ? '' : console.log('ThingsToCarry', err);
												let tempThingsToCarry = [];
												for (let j = 0; j < res.length; j++) {
													tempThingsToCarry.push({
														text: res[j].text
													});
												}
												tempCamp['thingsToCarry'] = tempThingsToCarry;
												getExclusions(camps[i].uuid, (err, res) => {
													!false ? '' : console.log('Exclusions', err);
													let tempExclusions = [];
													for (let j = 0; j < res.length; j++) {
														tempExclusions.push({ text: res[j].text });
													}
													tempCamp['exclusions'] = tempExclusions;
													getNotes(camps[i].uuid, (err, res) => {
														!false ? '' : console.log('Notes', err);
														let tempNotes = [];
														for (let j = 0; j < res.length; j++) {
															tempNotes.push(res[j].note);
														}
														tempCamp['notes'] = tempNotes;
														tempCamp['currency'] = camps[i].base_currency;
														getCampInfo(camps[i].uuid, (err, res) => {
															!false ? '' : console.log('CamoInfo', err);
															tempCamp['checkIn'] = res[0].checkin;
															tempCamp['checkOut'] = res[0].checkout;
															getLocation(camps[i].uuid, (err, res) => {
																!false
																	? ''
																	: console.log('Location-CampInfo', err);
																tempCamp['location'] = {
																	code: predefinedLocation[res[0].city].code,
																	name: predefinedLocation[res[0].city].name,
																	state: predefinedLocation[res[0].city].state,
																	region:
																		predefinedLocation[res[0].city].region,
																	country: 'India'
																};
																getAddress(camps[i].uuid, (err, res) => {
																	!false ? '' : console.log('Address', err);
																	if (
																		res[0].latitude.includes(' ') ||
																		res[0].longitude.includes(' ') ||
																		res[0].latitude.includes(',') ||
																		res[0].longitude.includes(',') ||
																		res[0].longitude.includes('°')
																	) {
																		tempCamp['address'] = {
																			locality: res[0].locality,
																			coordinates: [
																				res[0].longitude
																					.trim()
																					.split(',')[0]
																					.split(' ')[0]
																					.replace(/,/, '')
																					.replace("73°29'43.6", '73.29436'),
																				res[0].latitude
																					.trim()
																					.split(',')[0]
																					.split(' ')[0]
																					.replace(/,/, '')
																			],
																			pincode: res[0].pincode,
																			city: tempCamp.location.name,
																			state: tempCamp.location.state,
																			country: 'India'
																		};
																	} else {
																		tempCamp['address'] = {
																			locality: res[0].locality,
																			coordinates: [
																				res[0].longitude.trim(),
																				res[0].latitude.trim()
																			],
																			pincode: res[0].pincode,
																			city: tempCamp.location.name,
																			state: tempCamp.location.state,
																			country: 'India'
																		};
																	}
																});
																getPolicy(camps[i].uuid, (err, res) => {
																	!false ? '' : console.log('Policy', err);
																	tempCamp['policy'] = res[0].policy;
																});

																getRoom(camps[i].uuid, (err, res) => {
																	!false ? '' : console.log('Room', err);
																	let tempRooms = [];
																	for (let j = 0; j < res.length; j++) {
																		let tempRoom = {};
																		let room_type_id = res[j].room_type_id;
																		getCount(camps[i].uuid, (err, res) => {
																			!false
																				? ''
																				: console.log('Room Count', err);
																			tempRoom['count'] = res[0].total;
																		});
																		getCampRoomTypeRates(
																			room_type_id,
																			(err, res) => {
																				!false
																					? ''
																					: console.log('RoomTypeRates', err);
																				let tempPrice = new Map();
																				for (let k = 0; k < res.length; k++) {
																					tempPrice.set(
																						res[k].sharing_type_value,
																						(res[k].rate - 0.2 * res[k].rate) /
																							parseInt(
																								res[k].sharing_type_value
																							)
																					);
																				}
																				tempRoom['prices'] = mapToObj(
																					tempPrice
																				);
																				getCampRoomAmenities(
																					room_type_id,
																					(err, res) => {
																						!false
																							? ''
																							: console.log(
																									'RoomAmenities',
																									err
																							  );
																						let tempRoomAmenities = [];
																						for (
																							let k = 0;
																							k < res.length;
																							k++
																						) {
																							tempRoomAmenities.push({
																								text: res[k].amenity,
																								icon: res[k].value
																							});
																						}
																						tempRoom[
																							'amenities'
																						] = tempRoomAmenities;
																						getCampRoomTypes(
																							room_type_id,
																							(err, res) => {
																								!false
																									? ''
																									: console.log(
																											'RoomTypes',
																											err
																									  );
																								for (
																									let k = 0;
																									k < res.length;
																									k++
																								) {
																									tempRoom['washroom'] =
																										res[k].washroom;
																									tempRoom['type'] =
																										res[k].type;
																									tempRoom['maximumOccupancy'] =
																										res[k].maximumOccupancy;
																									// tempRoom[
																									// 	'minimumOccupancy'
																									// ] = 2;
																									tempRoom['childDiscount'] =
																										res[k].childDiscount;
																								}
																								tempRooms.push(tempRoom);
																								tempCamp['rooms'] = tempRooms;
																							}
																						);
																					}
																				);
																			}
																		);
																	}
																	getActivities(camps[i].uuid, (err, res) => {
																		!false
																			? ''
																			: console.log('Activities', err);
																		let tempActivities = [];
																		for (let k = 0; k < res.length; k++) {
																			tempActivities.push({
																				text: res[k].title,
																				description: res[k].description,
																				icon: res[k].value
																			});
																		}
																		tempCamp['activities'] = tempActivities;
																		getSEO(camps[i].uuid, (err, res) => {
																			!false ? '' : console.log('seo', err);
																			if (res.length != 0) {
																				tempCamp['seo'] = {
																					title: res[0].title,
																					description: res[0].description
																				};
																			} else {
																				tempCamp['seo'] = {
																					title: camps[i].display_name,
																					description: camps[i].display_name
																				};
																			}
																			getTerrain(camps[i].uuid, (err, res) => {
																				let tempTerrains = [];
																				for (let k = 0; k < res.length; k++) {
																					tempTerrains.push({
																						text: res[k].name
																					});
																				}
																				tempCamp['terrains'] = tempTerrains;
																				setTimeout(function() {
																					axios
																						.post(
																							'https://admin.mounty.co/camps/v1',
																							tempCamp,
																							{
																								headers: {
																									'Content-Type':
																										'application/json',
																									Authorization:
																										'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
																								}
																							}
																						)
																						.then(function(response) {
																							if (response.data.error) {
																								console.log(
																									'\n\n',
																									camps[i].display_name
																								);
																								console.log(response.data);
																								count++;
																							} else {
																								count++;
																								campJson[camps[i].uuid] =
																									response.data.data;
																								if (count == total) {
																									var jsonContent = JSON.stringify(
																										campJson
																									);
																									fs.writeFile(
																										'campJSON.json',
																										jsonContent,
																										'utf8',
																										function(err) {
																											if (err) {
																												console.log(
																													'An error occured while writing JSON Object to File.'
																												);
																											}

																											console.log('Done');
																										}
																									);
																								}
																							}
																						})
																						.catch(function(error) {
																							count++;
																							console.log(
																								'\n\n',
																								camps[i].display_name
																							);
																							if (error.response) {
																								console.log(
																									error.response.data
																										.errorDetails
																								);
																							} else {
																								console.log(error);
																							}

																							if (count == total) {
																								var jsonContent = JSON.stringify(
																									campJson
																								);
																								fs.writeFile(
																									'campJSON.json',
																									jsonContent,
																									'utf8',
																									function(err) {
																										if (err) {
																											console.log(
																												'An error occured while writing JSON Object to File.'
																											);
																										}

																										console.log('Done');
																									}
																								);
																							}
																						});
																				}, 1000);
																			});
																		});
																	});
																});
															});
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			}
		});
	}
});

function getPredefinedLocations(res) {
	axios
		.get('https://admin.mounty.co/locations/v1', {
			headers: {
				'Content-Type': 'application/json',
				Authorization:
					'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
			}
		})
		.then(function(response) {
			return res(null, response.data.data);
		})
		.catch(function(error) {
			!false ? '' : console.log(error);
		});
}

function getActivities(req, res) {
	client.query(
		`SELECT activities.title,activities.description,icons.value FROM camp_activities JOIN activities ON camp_activities.activity_id = activities.id JOIN icons ON activities.icon_id = icons.id where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getSEO(req, res) {
	client.query(
		`SELECT title,description FROM camp_seos where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getPolicy(req, res) {
	client.query(
		`SELECT policy FROM camp_cancellation_policies where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getTerrain(req, res) {
	client.query(
		`SELECT tag_types.name FROM camp_tags JOIN tag_types ON camp_tags.tag_type_id = tag_types.id where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getCount(req, res) {
	client.query(
		`SELECT COUNT(room_no) AS total FROM camp_rooms where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getRoom(req, res) {
	client.query(
		`SELECT DISTINCT room_type_id FROM camp_rooms where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getCampRoomTypes(req, res) {
	client.query(
		`SELECT washroom,max_occupancy AS maximumOccupancy,name AS type,child_percent AS childDiscount FROM camp_room_types where id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getCampRoomTypeRates(req, res) {
	client.query(
		`SELECT sharing_type_value,rate FROM camp_room_type_rates where room_type_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getCampRoomAmenities(req, res) {
	client.query(
		`SELECT room_amenities.amenity,icons.value FROM camp_room_amenities JOIN room_amenities ON camp_room_amenities.room_amenity_id = room_amenities.id JOIN icons ON room_amenities.icon_id = icons.id where room_type_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getLocation(req, res) {
	client.query(
		`SELECT locations.city,states.name from camps JOIN locations ON locations.id = camps.location_id JOIN states ON states.id = locations.state_id where uuid = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getAddress(req, res) {
	client.query(
		`SELECT longitude,locality,pincode,latitude,longitude from camp_locations where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getCampInfo(req, res) {
	client.query(
		`SELECT checkin,checkout from camp_infos where camp_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getNotes(req, res) {
	client.query(
		`SELECT description AS note from camp_notes where camp_id = ?`,
		req,
		function(err, notes, fields) {
			if (err) throw err;
			else {
				return res(null, notes);
			}
		}
	);
}

function getExclusions(req, res) {
	client.query(
		`SELECT description AS text from camp_exclusions where camp_id = ?`,
		req,
		function(err, exclusion, fields) {
			if (err) throw err;
			else {
				return res(null, exclusion);
			}
		}
	);
}

function getThingsToCarry(req, res) {
	client.query(
		`SELECT thing AS text from camp_things_to_carries where camp_id = ?`,
		req,
		function(err, direction, fields) {
			if (err) throw err;
			else {
				return res(null, direction);
			}
		}
	);
}

function getDirection(req, res) {
	client.query(
		`SELECT type AS mode,point AS description from camp_directions where camp_id = ?`,
		req,
		function(err, direction, fields) {
			if (err) throw err;
			else {
				return res(null, direction);
			}
		}
	);
}

function getInclusions(req, res) {
	client.query(
		`SELECT description AS text from camp_inclusions where camp_id = ?`,
		req,
		function(err, inclusions, fields) {
			if (err) throw err;
			else {
				return res(null, inclusions);
			}
		}
	);
}

function getPhotos(req, res) {
	client.query(
		`SELECT photo AS url,caption AS caption from camp_photos where camp_id = ?`,
		req,
		function(err, photos, fields) {
			if (err) throw err;
			else {
				return res(null, photos);
			}
		}
	);
}

function getVideos(req, res) {
	client.query(
		`SELECT video AS url,caption AS caption from camp_videos where camp_id = ?`,
		req,
		function(err, videos, fields) {
			if (err) throw err;
			else {
				return res(null, videos);
			}
		}
	);
}

function getDescription(req, res) {
	client.query(
		`SELECT description from camp_descriptions where camp_id = ?`,
		req,
		function(err, description, fields) {
			if (err) throw err;
			else {
				return res(null, description);
			}
		}
	);
}

function getAmenities(req, res) {
	client.query(
		`SELECT amenities.amenity AS text, icons.value AS icon FROM camp_amenities JOIN amenities ON camp_amenities.amenity_id = amenities.id JOIN icons ON amenities.icon_id = icons.id where camp_id = ?`,
		req,
		function(err, amenities, fields) {
			if (err) throw err;
			else {
				return res(null, amenities);
			}
		}
	);
}

function getHighlights(req, res) {
	client.query(
		`SELECT description from camp_highlights where camp_id = ?`,
		req,
		function(err, description, fields) {
			if (err) throw err;
			else {
				return res(null, description);
			}
		}
	);
}

function mapToObj(inputMap) {
	let obj = {};

	inputMap.forEach(function(value, key) {
		obj[key] = value;
	});

	return obj;
}
