const client = require('./db_helper');
const axios = require('axios');

client.query(
  'SELECT amenities.amenity AS name, icons.value AS icon FROM amenities JOIN icons ON amenities.icon_id = icons.id',
  function(err, result, fields) {
    if (err) throw err;
    else {
      console.log('Extra to be posted as amenity', result.length);
      for (let i = result.length - 1; i >= 0; --i) {
        const extra = {
          type: 'amenities',
          text: result[i].name,
          icon: result[i].icon
        };
        axios
          .post('http://dev-kube.admin.mounty.co/camps/extras', extra, {
            headers: {
              'Content-Type': 'application/json',
              Authorization:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }
);

client.end();
