const axios = require('axios');

const client = require('./db_helper');

let codeCount = 1000;

client.query(
	'SELECT locations.city AS city, locations.image AS image, states.region, states.name AS state FROM locations JOIN states ON locations.state_id = states.id',
	function(err, result, fields) {
		if (err) throw err;

		postLocations(result);
	}
);

client.end();

function postLocations(locations) {
	for (location of locations) {
		axios
			.post(
				'http://dev-kube.admin.mounty.co/locations/v1',
				{
					name: location.city,
					state: location.state,
					media: {
						images: [
							{
								url:
									'https://s3.ap-south-1.amazonaws.com/media-mounty/' +
									location.image
							}
						]
					},
					seo: {
						slug: location.city
							.toLowerCase()
							.replace(/ /g, '-')
							.replace('(', '')
							.replace(')', '')
					},
					region: location.region,
					country: 'India',
					continent: 'Asia',
					coordinates: [0.0, 0.0],
					code: `LOC${codeCount}`
				},
				{
					headers: {
						'Content-Type': 'application/json',
						authorization:
							'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
					}
				}
			)
			.then(function(data) {
				console.log(data.data);
			})
			.catch(function(error) {
				console.log(error.response.data);
			});

		codeCount++;
	}
}
