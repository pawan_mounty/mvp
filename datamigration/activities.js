const client = require('./db_helper');
const axios = require('axios');

client.query(
  'SELECT activities.title AS text, icons.value AS icon, description FROM activities JOIN icons ON activities.icon_id = icons.id',
  function(err, result, fields) {
    if (err) throw err;
    else {
      console.log('Extra to be posted as activities', result.length);
      for (let i = result.length - 1; i >= 0; --i) {
        const extra = {
          type: 'activities',
          text: result[i].text,
          description: result[i].description,
          icon: result[i].icon
        };
        axios
          .post('http://dev-kube.admin.mounty.co/camps/extras', extra, {
            headers: {
              'Content-Type': 'application/json',
              Authorization:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
            }
          })
          .catch(error => {
            console.log(error);
          });
        // axios
        //   .post('http://localhost:3002/extras', extra)
        //   .then(response => {
        //     if (response.data.error) {
        //       console.log(response.data);
        //     }
        //   })
        //   .catch(error => {
        //     console.log(error);
        //   });
      }
    }
  }
);

client.end();
