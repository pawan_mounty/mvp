const client = require('./db_helper');
const axios = require('axios');
const fs = require('fs');

let count = 0;
let toBePosted = 0;

client.query(
	'SELECT uuid,first_name,last_name,phone,email from camp_operators where active = 1',
	function(err, operators, fields) {
		if (err) throw err;
		else {
			let jsonData = {};
			toBePosted = operators.length;
			console.log('Camp Operator to be posted', toBePosted);
			for (let i = 0; i < operators.length; i++) {
				let tempOperator = {
					firstName: operators[i].first_name,
					lastName: operators[i].last_name,
					mobile: '+91' + operators[i].phone,
					email: operators[i].email,
					active: true
				};
				getAddress(operators[i].uuid, (err, res) => {
					tempOperator['address'] = {
						locality: res[0].locality,
						city: res[0].city,
						state: res[0].state,
						country: 'India',
						pincode: res[0].pincode
					};
					getBankDetails(operators[i].uuid, (err, res) => {
						tempOperator['bank'] = {
							name: res[0].bankName,
							accountType: res[0].type,
							beneficiary: res[0].beneficiaryName,
							accountNumber: res[0].accountNumber,
							ifsc: res[0].ifsc
						};
						tempOperator['legal'] = {
							businessType: 'TBD',
							businessName: 'TBD'
						};
						axios
							.post('https://admin.mounty.co/operators/v1', tempOperator, {
								headers: {
									'Content-Type': 'application/json',
									authorization:
										'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
								}
							})
							.then(function(response) {
								jsonData[operators[i].uuid] = response.data.data;
								if (response.error) {
									console.log(response.data);
								}

								count++;

								if (count == toBePosted) {
									var jsonContent = JSON.stringify(jsonData);
									fs.writeFile('output2.json', jsonContent, 'utf8', function(
										err
									) {
										if (err) {
											console.log(
												'An error occured while writing JSON Object to File.'
											);
											// return console.log(err);
										}

										console.log('Finally...');
									});
								}
							})
							.catch(function(error) {
								console.log(
									'\n\n\n\n\n\n\n',
									operators[i].first_name + operators[i].last_name
								);
								console.log(error.response.data.errorDetails);
							});
						// axios
						// 	.post(
						// 		'http://admin.mounty.co/operators/v1',
						// 		tempOperator
						// 	)
						// 	.then(function(response) {
						// 		jsonData[operators[i].uuid] = response.data.data;
						// 		console.log(response.data);
						// 	})
						// 	.catch(function(error) {
						// 		console.log("\n\n\n\n\n\n\n",operators[i].first_name + operators[i].last_name);
						// 		console.log(error.response.data.errorDetails);
						// 	});
					});
				});
			}
		}
	}
);

function getAddress(req, res) {
	client.query(
		`SELECT camp_operator_addresses.address AS locality,camp_operator_addresses.city,camp_operator_addresses.pincode,states.name AS state FROM camp_operator_addresses JOIN states ON camp_operator_addresses.state_id = states.id where operator_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}

function getBankDetails(req, res) {
	client.query(
		`SELECT bank_name AS bankName,ac_type AS type,beneficiary_name AS beneficiaryName,ac_no AS accountNumber,ifsc_code AS ifsc FROM camp_operator_bank_details where operator_id = ?`,
		req,
		function(err, data, fields) {
			if (err) throw err;
			else {
				return res(null, data);
			}
		}
	);
}
