const client = require('./db_helper');
const axios = require('axios');

client.query(
  'SELECT room_amenities.amenity AS text, icons.value AS icon FROM room_amenities JOIN icons ON room_amenities.icon_id = icons.id',
  function(err, result, fields) {
    if (err) throw err;
    else {
      console.log('Extra to be posted as room amenity', result.length);
      for (let i = result.length - 1; i >= 0; --i) {
        const extra = {
          type: 'roomAmenities',
          text: result[i].text,
          icon: result[i].icon
        };
        axios
          .post('http://dev-kube.admin.mounty.co/camps/extras', extra, {
            headers: {
              'Content-Type': 'application/json',
              Authorization:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
            }
          })
          .catch(error => {
            console.log(error);
          });
        // axios
        //   .post('http://localhost:3002/extras', extra)
        //   .then(response => {
        //     if (response.data.error) {
        //       console.log(response.data);
        //     }
        //   })
        //   .catch(error => {
        //     console.log(error);
        //   });
      }
    }
  }
);

client.end();
