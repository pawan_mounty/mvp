const client = require('./db_helper');
const axios = require('axios');

client.query(
  'SELECT DISTINCT thing as text FROM camp_things_to_carries',
  function(err, result, fields) {
    if (err) throw err;
    else {
      console.log('Extra to be posted as Things to carry', result.length);
      for (let i = result.length - 1; i >= 0; --i) {
        const extra = {
          type: 'thingsToCarry',
          text: result[i].text
        };
        axios
          .post('http://dev-kube.admin.mounty.co/camps/extras', extra, {
            headers: {
              'Content-Type': 'application/json',
              Authorization:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }
);

client.end();
