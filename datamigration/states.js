const client = require('./db_helper');

let country = {
  name: 'India',
  continent: 'Asia',
  currency: {
    name: 'Rupees',
    symbol: '$'
  }
};

client.query('Select name,region from states', function(err, result, fields) {
  let stateList = [];
  if (err) throw err;
  else {
    for (let i = result.length - 1; i >= 0; --i) {
      const state = {
        name: result[i].name,
        region: result[i].region
      };
      stateList.push(state);
    }
    country['states'] = stateList;
    console.log(country);
  }
});

client.end();
