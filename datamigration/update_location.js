const axios = require('axios');

const client = require('./db_helper');

client.query(
  'Select locations.city as location, location_seos.title as title, location_seos.description as description from locations JOIN location_seos on locations.id = location_seos.location_id',
  function(err, result, fields) {
    if (err) throw err;

    getAllDATA(result);
  }
);

client.end();

function getAllDATA(sql) {
  for (const sqlData of sql) {
    const filters = [{ key: 'name', value: sqlData.location }];
    axios({
      method: 'get',
      url: `http://dev-kube.admin.mounty.co/locations/v1/`,
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
      },
      params: { filters: JSON.stringify(filters) }
    })
      .then(async response => {
        if (response.error) {
        } else {
          if (response.data.data.length != 0) {
            getSingleData(
              response.data.data[0]._id,
              sqlData.title,
              sqlData.description
            );
          }
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }
}

function getSingleData(id, title, description) {
  axios({
    method: 'get',
    url: `http://dev-kube.admin.mounty.co/locations/v1/${id}`,
    headers: {
      'Content-Type': 'application/json',
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
    }
  })
    .then(async response => {
      if (response.error) {
      } else {
        const campTitle = response.data.data.campSeo.title;
        const campDecription = response.data.data.campSeo.description;

        let data = response.data.data;

        data.campSeo.title = title || campTitle;
        data.campSeo['description'] = description || campDecription;

        updateCampSeo(id, data);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

let count = 0;

function updateCampSeo(id, payload) {
  axios
    .request({
      method: 'PUT',
      url: `http://dev-kube.admin.mounty.co/locations/v1/${id}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY2NjkyMjB9.Pr1k9DCW23Nwr1uOA4wZK20cZosQHb52cPhb6tznDIE'
      },
      data: payload
    })
    .then(response => {
      if (response.error) {
        console.log('error');
      } else {
        console.log('done\t\t', count);
        count++;
      }
    })
    .catch(error => {
      console.log('error');
    });
}
