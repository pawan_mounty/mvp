const campData = require('./campJSON.json');
const userData = require('../dataMigrationUser/output.json');
const client = require('./db_helper');
const axios = require('axios');

let count = 0;
let total = 0;

client.query('SELECT * from camp_bookings', function(err, bookings, fields) {
  if (err) {
    console.log(err);
  } else {
    console.log('Bookings to be posted', bookings.length);
    total = bookings.length;
    for (let i = 0; i < bookings.length; i++) {
      let booking = {
        reference: bookings[i].booking_ref,
        status: bookings[i].status,
        platform: 'TBD',
        products: [
          {
            category: 'camp',
            checkInDate: bookings[i].booking_date,
            checkOutDate: bookings[i].end_date,
            guests: {
              adults: bookings[i].adults,
              children: bookings[i].children
            }
          }
        ],
        roomArrangement: {
          adults: bookings[i].adults,
          children: bookings[i].children
        }
      };
      if (userData[bookings[i].customer_id]) {
        booking['userId'] = userData[bookings[i].customer_id].id;
      }
      if (campData[bookings[i].camp_id]) {
        booking['products'][0]['id'] = campData[bookings[i].camp_id];
      }
      getContact(bookings[i].uuid, (err, res) => {
        booking['contact'] = {
          name: res[0].name,
          mobile: res[0].phone,
          email: res[0].email
        };
        getPayments(bookings[i].uuid, (err, res) => {
          let tempPayment = [];
          for (let j = 0; j < res.length; j++) {
            tempPayment.push({
              mode: res[j].mode,
              transactionId: res[j].invoice_id,
              via: res[j].via,
              amount: res[j].amount
            });
          }
          booking['payments'] = tempPayment;
          getBilling(bookings[i].uuid, (err, res) => {
            booking['billing'] = {
              subtotal: res[0].subtotal,
              walletDiscount: res[0].walletDiscount,
              netAmount: res[0].netAmount
            };
            setTimeout(() => {
              console.log(booking);
            }, 1000);
            console.log(booking);
          });
        });
      });
    }
  }
});

function getBilling(req, res) {
  client.query(
    `SELECT discounted_amount AS subtotal,use_wallet AS walletDiscount, total_amount AS netAmount from camp_booking_amounts where booking_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getContact(req, res) {
  client.query(
    `SELECT name,phone,email from camp_booking_customers where booking_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}

function getPayments(req, res) {
  client.query(
    `SELECT amount,mode,via,invoice_id from camp_booking_payments where booking_id = ?`,
    req,
    function(err, data, fields) {
      if (err) throw err;
      else {
        return res(null, data);
      }
    }
  );
}
