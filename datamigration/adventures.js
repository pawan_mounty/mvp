const client = require('./db_helper');

client.query(
  'SELECT adventures.title AS name, adventures.description AS description, icons.value AS icon FROM adventures JOIN icons ON adventures.icon_id = icons.id',
  function(err, result, fields) {
    if (err) throw err;
    else {
      for (let i = result.length - 1; i >= 0; --i) {
        console.log(result[i]);
      }
    }
  }
);

client.end();
