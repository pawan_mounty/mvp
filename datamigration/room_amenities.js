const client = require('./db_helper');

client.query(
  'SELECT room_amenities.amenity AS name, icons.value AS icon FROM room_amenities JOIN icons ON room_amenities.icon_id = icons.id',
  function(err, result, fields) {
    if (err) throw err;
    else {
      for (let i = result.length - 1; i >= 0; --i) {
        console.log(result[i]);
      }
    }
  }
);

client.end();
