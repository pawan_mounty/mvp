const client = require('./db_helper');
const axios = require('axios');

let count = 0;
let total = 0;

client.query(
  'SELECT legal_name as businessName, business_registration_type as businessType, pan, gstin, owner_phone as mobile, owner_name as name from camp_agreements',
  (err, docs, fields) => {
    if (err) {
      console.log(err);
    } else {
      total = docs.length;
      for (let i = 0; i < docs.length; i++) {
        const legal = {
          legal: {
            businessType: docs[i].businessType,
            businessName: docs[i].businessName,
            pan: docs[i].pan || undefined,
            gstin: docs[i].gstin || undefined
          }
        };

        axios
          .put(`http://localhost:3008/operators/v1/91${docs[i].mobile}`, legal)
          .then(function(response) {
            if (response.error) {
              console.log(response.data);
            }
            count++;
            if (count == total) {
              console.log('done');
            }
          })
          .catch(function(error) {
            count++;
            console.log('\n\n\n\n\n\n\n', docs[i].name);
            console.log(error.response.data.errorDetails);
          });
      }
    }
  }
);
