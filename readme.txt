For excel data :- 

1. cd excel
2. node operator.js //wait for the process to complete it creates all operator and returns operator id which will be saved on output1.json file
3. node adventure.js // it creates adventure using operator id from output1.json

For MySQL data :-

1. cd datamigration
2. node operator.js //wait for the process to complete it creates all operator and returns operator id which will be saved on output2.json file and use ctrl+c to end the process after sometimes
3. node camp.js // it creates camps using operator id from output2.json and use ctrl+c to end the process after sometimes and will create campJSON.json file as output as well.
