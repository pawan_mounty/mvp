let kue = require('kue');
let queue = kue.createQueue();
var sleep = require('system-sleep');

queue.process(`download`, function(job, done) {
  console.log(`Working on job ${job.id}`);
  console.log(job.data);
  downloadFile(job.data.file, done);
});

function downloadFile(file, done) {
  sleep(5000);

  console.log(`Downloading file : ${file}`);
  sleep(5000);
  console.log(`Download Complete`);
  done();
}
