const axios = require('axios');

axios
  .get('https://operator.mounty.co/platform/v1/countries')
  .then(response => {
    if (response.data.error) {
      console.log(response.data);
    }
    postDataOfState(response.data.data[0].states);
  })
  .catch(error => {
    console.log(error);
  });

function postDataOfState(data) {
  for (const value of data) {
    const postData = {
      name: value.name,
      country: {
        name: 'India',
        code: 'COUNTRY1000',
        slug: 'india'
      },
      coordinates: [0, 0],
      media: {
        images: [
          {
            url:
              'https://media-mounty.s3.amazonaws.com/images/6d601948-174a-4cb9-9271-97d367ee0687',
            caption: 'caption',
            alt: 'alt'
          },
          {
            url:
              'https://s3.ap-south-1.amazonaws.com/media-mounty/media/camps/camping-in-manali-himachal-pradesh-india-hill-everest/photos/la6tXTQfAjOTS59kpU2kCTHE8fMK4R1OEw2oZGfr.webp',
            caption: 'caption',
            alt: 'alt'
          },
          {
            url:
              'https://s3.ap-south-1.amazonaws.com/media-mounty/media/camps/camping-in-manali-himachal-pradesh-india-hill-everest/photos/vS0YVdAAIHkSpzdWTek6KWYdoozNVDH2t68xQVU2.webp',
            caption: 'caption',
            alt: 'alt'
          },
          {
            url:
              'https://s3.ap-south-1.amazonaws.com/media-mounty/media/camps/camping-in-manali-himachal-pradesh-india-hill-everest/photos/AlLvuvgdANZEi5HbUh8Oqw433g4MGaCGiIcktk8b.webp',
            caption: 'caption',
            alt: 'alt'
          },
          {
            url:
              'https://s3.ap-south-1.amazonaws.com/media-mounty/media/camps/camping-in-manali-himachal-pradesh-india-hill-everest/photos/xYAjIU6vs4HJQQPRIDNZ94d136uojY47gtJ97Kcv.webp',
            caption: 'caption',
            alt: 'alt'
          }
        ]
      }
    };

    axios
      .post('http://localhost:3001/states/v1', postData)
      .then(response => {
        if (response.data.error) {
          console.log(response.data);
        }
      })
      .catch(error => {
        console.log(error.response.data);
      });
  }
}
